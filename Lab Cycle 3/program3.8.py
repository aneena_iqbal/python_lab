n=int(input("Enter the number of terms: "))
sum=0
def fact(n):
	if n==1:
		return 1
	else:
		return n*fact(n-1)
for i in range(1,n+1):
	f=fact(i)
	sum=sum+(pow(i,i)/f)
print("Sum of series is ",sum)