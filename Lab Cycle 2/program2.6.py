list1 = [-1,2,3,4,-5]
word='apple'
pos=[num for num in list1 if num >= 0]
print("\nPositive numbers in the list: ", *pos) 
squares = [number**2 for number in list1]
print("\nSquares of the numbers in the list: ", squares)
vowels = [v for v in word if v in "aeiouAEIOU"]
print("\nVowels in the word: ", vowels)
ordinal=[ord(i) for i in word]
print("\nOrdinal value of each element in word:  ", ordinal)
